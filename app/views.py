from django.shortcuts import render
from django.contrib.auth.models import User
from faker import Faker

# Create your views here.
def index(request):
    text=' wsjfgnjklsf;ans'
    user = User.objects.first()
    return render(request, 'app/index.html', context= {'text': text, 'user': user})

def list(request):
    result_list = [1,2,3,4,5,6,7,8,9,0,'ergf','fsag','awsedf']
    # fake = Faker()
    # # User.objects.bulk_create([User(username=fake.user_name()) for i in range(0,50)]) 批量創建50個帳戶
    # results = []
    # for i in range(0,50):
    #     one_user = User(username = fake.user_name())
    #     results.append(one_user)
    # User.objects.bulk_create(results) 
    users = User.objects.filter(id__gt=1)
    return render(request, 'app/list.html', context= { 'result_list': result_list, 'users': users })

def ifendif(request):
    users = User.objects.filter(id__gt=1)
    return render(request, 'app/if-endif.html', context = { 'users': users })